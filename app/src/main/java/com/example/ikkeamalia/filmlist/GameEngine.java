package com.example.ikkeamalia.filmlist;

import android.content.Context;

import com.example.ikkeamalia.filmlist.view.sudokugrid.GameGrid;
import com.example.ikkeamalia.filmlist.view.sudokugrid.SudokuCell;

/**
 * Created by Krisnanda on 17/03/2019.
 */

public class GameEngine {
    private static GameEngine instance;

    private GameGrid grid = null;

    private SudokuCell[][] Sudoku = new SudokuCell[9][9];

    private Context context;

    private int selectedPosX = -1, selectedPosY = -1;

    private GameEngine(){}


    public static GameEngine getInstance(){
        if( instance == null ){
            instance = new GameEngine();
        }
        return instance;
    }

    public void createGrid( Context context ){
        int[][] Sudoku = SudokuGenerator.getInstance().generateGrid();
        Preference.getInstance().setArrayPrefs(Sudoku,context);
        Sudoku = SudokuGenerator.getInstance().removeElements(Sudoku);
        grid = new GameGrid(context);
        grid.setGrid(Sudoku);
    }

    public void solveMe( Context context ){
        int[][] SolvedSudoku = Preference.getInstance().getArrayPrefs(context);
        grid = new GameGrid(context);
        grid.setGrid(SolvedSudoku);
    }

    public GameGrid getGrid(){
        return grid;
    }

    public void setSelectedPosition( int x , int y ){
        selectedPosX = x;
        selectedPosY = y;

       // drawSelectedCell();
    }


    public void setNumber( int number ){
        if( selectedPosX != -1 && selectedPosY != -1 ){
            grid.setItem(selectedPosX,selectedPosY,number);
        }
        grid.checkGame();
    }
}
