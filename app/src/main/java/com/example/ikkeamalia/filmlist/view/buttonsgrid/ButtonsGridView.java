package com.example.ikkeamalia.filmlist.view.buttonsgrid;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;

import com.example.ikkeamalia.filmlist.R;

/**
 * Created by Krisnanda on 17/03/2019.
 */

public class ButtonsGridView extends GridView {
    public ButtonsGridView(Context context, AttributeSet attrs) {
        super(context, attrs);

        ButtonsGridViewAdapter gridViewAdapter = new ButtonsGridViewAdapter(context);

        setAdapter(gridViewAdapter);
    }

    class ButtonsGridViewAdapter extends BaseAdapter {

        private Context context;

        public ButtonsGridViewAdapter(Context context) {
            this.context = context;
        }

        @Override
        public int getCount() {
            return 9;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }



        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;

            if( v == null ){
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                v = inflater.inflate(R.layout.button, parent , false);

                NumberButton btn;
                btn = (NumberButton)v;
                btn.setTextSize(22);
                btn.setTextColor(Color.parseColor("#324d5c"));
                btn.setId(position);

                if( position != 9 ){
                    btn.setText(String.valueOf(position + 1));
                    btn.setNumber(position + 1);
                }
                return btn;
            }

            return v;
        }

    }
}
