package com.example.ikkeamalia.filmlist;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.UserManager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.concurrent.TimeUnit;

/**
 * Created by Krisnanda on 17/03/2019.
 */

public class MainActivity extends Activity {

    Button mulaiLagi, solveMe;
    TextView timer;
    int solveme = 0;
    ImageView play, pause;
    SudokuCounter counter;
    Long s1;
    public static boolean timerPaused=false;

    public static MainActivity instance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mulaiLagi = findViewById(R.id.mulaiLagi);
        timer     = findViewById(R.id.timer);
        solveMe   = findViewById(R.id.solveMe);
        play      = findViewById(R.id.play);
        pause     = findViewById(R.id.pause);

        instance = this;

        Bundle bundle = getIntent().getExtras();
        if (bundle!=null){
            solveme = bundle.getInt("solveMe");
        }

        if (solveme==1){
            GameEngine.getInstance().solveMe(this);
            play.setVisibility(View.VISIBLE);
            pause.setVisibility(View.GONE);

            timer.setText(String.format("%2d : %2d ",
                    TimeUnit.MILLISECONDS.toMinutes( Preference.getInstance().getLastTick(this)),
                    TimeUnit.MILLISECONDS.toSeconds(Preference.getInstance().getLastTick(this)) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(Preference.getInstance().getLastTick(this)))));
        } else {
            GameEngine.getInstance().createGrid(this);
            counter= new SudokuCounter(120000,1000);
            counter.start();
        }

        mulaiLagi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent refresh = new Intent(MainActivity.this, MainActivity.class);
                refresh.putExtra("solveMe", 0);
                refresh.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(refresh);
            }
        });

        solveMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Preference.getInstance().setLastTick(s1, MainActivity.this);
                Intent refresh = new Intent(MainActivity.this, MainActivity.class);
                refresh.putExtra("solveMe", 1);
                refresh.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(refresh);
            }
        });

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timerPaused = false;
                pause.setVisibility(View.VISIBLE);
                play.setVisibility(View.GONE);

                if (solveme!=1) {
                    counter = new SudokuCounter(s1, 1000);
                    counter.start();
                }else {
                    counter = new SudokuCounter(Preference.getInstance().getLastTick(MainActivity.this), 1000);
                    counter.start();
                }
            }
        });

        pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timerPaused = true;
                play.setVisibility(View.VISIBLE);
                pause.setVisibility(View.GONE);

                counter.cancel();
            }
        });

//        new CountDownTimer(120000, 1000) {
//            // adjust the milli seconds here
//            public void onTick(long millisUntilFinished) {
//                timer.setText(String.format("%2d : %2d ",
//                        TimeUnit.MILLISECONDS.toMinutes( millisUntilFinished),
//                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
//                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
//            }
//
//
//            public void onFinish() {
//                final AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
//                alertDialog.setTitle("Waktu anda habis ! ");
//                alertDialog.setMessage("Mulai Lagi ?  ");
//                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "YA", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        alertDialog.dismiss();
//                        mulai();
//                    }
//                });
//                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "TIDAK", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        alertDialog.dismiss();
//                       finish();
//                    }
//                });
//                alertDialog.show();
//            }
//        }.start();
    }

    private void printSudoku(int Sudoku[][]) {
        for (int y = 0; y < 9; y++) {
            for (int x = 0; x < 9; x++) {
                System.out.print(Sudoku[x][y] + "|");
            }
            System.out.println();
        }
    }

    public void mulai(){
        Intent refresh = new Intent(MainActivity.this, MainActivity.class);
        refresh.putExtra("solveMe", 0);
        refresh.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(refresh);
    }

    public void selesaiMain(){
        final AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle("Selamat anda berhasil :) ");
        alertDialog.setMessage("Mulai Lagi ?  ");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "YA", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
                mulai();
            }
        });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "TIDAK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
                finish();
            }
        });
        alertDialog.show();
    }

    public void gameOver(){
        final AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle("GAME OVER ");
        alertDialog.setMessage("Angka yang anda isikan salah, ingin main lagi ?  ");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "YA", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
                mulai();
            }
        });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "TIDAK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
                finish();
            }
        });
        alertDialog.show();
    }

    public class SudokuCounter extends CountDownTimer
    {
        public SudokuCounter(long millisInFuture, long countDownInterval)
        {
            super(millisInFuture, countDownInterval);
        }
        @Override
        public void onFinish()
        {
            final AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
            alertDialog.setTitle("Waktu anda habis ! ");
            alertDialog.setMessage("Mulai Lagi ?  ");
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "YA", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    alertDialog.dismiss();
                    mulai();
                }
            });
            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "TIDAK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    alertDialog.dismiss();
                    finish();
                }
            });
            alertDialog.show();
        }
        @Override
        public void onTick(long millisUntilFinished)
        {
            s1=millisUntilFinished;
            timer.setText(String.format("%2d : %2d ",
                    TimeUnit.MILLISECONDS.toMinutes( millisUntilFinished),
                    TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
        }
    }


}
