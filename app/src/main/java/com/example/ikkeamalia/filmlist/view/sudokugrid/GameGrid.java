package com.example.ikkeamalia.filmlist.view.sudokugrid;

import android.content.Context;
import android.widget.AbsListView;
import android.widget.Toast;

import com.example.ikkeamalia.filmlist.GameEngine;
import com.example.ikkeamalia.filmlist.MainActivity;
import com.example.ikkeamalia.filmlist.RootActivity;
import com.example.ikkeamalia.filmlist.SudokuChecker;
import com.example.ikkeamalia.filmlist.SudokuGenerator;

/**
 * Created by Krisnanda on 17/03/2019.
 */

public class GameGrid {
    private static GameGrid instance;
    private SudokuCell[][] Sudoku = new SudokuCell[9][9];

    private Context context;
    public static boolean cellClicked=false;

    int cekGagal=0;
    private GameGrid(){}


    public static GameGrid getInstance(){
        if( instance == null ){
            instance = new GameGrid();
        }
        return instance;
    }

    public GameGrid( Context context ){
        this.context = context;
        for( int x = 0 ; x < 9 ; x++ ){
            for( int y = 0 ; y < 9 ; y++){
                if (x==0 || x==2 || x==4 || x==6 || x==8){
                    if (y%2==0){
                        Sudoku[x][y] = new SudokuCell(context, false, x, y);
                    } else {
                        Sudoku[x][y] = new SudokuCell(context, true,  x, y);
                    }
                } else if (x==1 || x==3 || x==5 || x==7){
                    if (y%2==0){
                        Sudoku[x][y] = new SudokuCell(context, true,  x, y);
                    } else {
                        Sudoku[x][y] = new SudokuCell(context, false,  x, y);
                    }
                }
            }
        }
    }


    public void setGrid( int[][] grid ){
        for( int x = 0 ; x < 9 ; x++ ){
            for( int y = 0 ; y < 9 ; y++){
                Sudoku[x][y].setInitValue(grid[x][y]);
                if( grid[x][y] != 0 ){
                    Sudoku[x][y].setNotModifiable();
                }
            }
        }
    }

//    public void drawSelectedCell(Context context, int x, int y){
//        if (x==0 || x==2 || x==4 || x==6 || x==8){
//            if (y%2==0){
//                Sudoku[x][y] = new SudokuCell(context, false, "cellClicked");
//            } else {
//                Sudoku[x][y] = new SudokuCell(context, true, "cellClicked");
//            }
//        } else if (x==1 || x==3 || x==5 || x==7){
//            if (y%2==0){
//                Sudoku[x][y] = new SudokuCell(context, true, "cellClicked");
//            } else {
//                Sudoku[x][y] = new SudokuCell(context, false, "cellClicked");
//            }
//        }
//        //Sudoku[x][y] = new SudokuCell(context, false, "cellClicked");
//        cellClicked = true;
//    }


    public SudokuCell[][] getGrid(){
        return Sudoku;
    }

    public SudokuCell getItem(int x , int y ){
        return Sudoku[x][y];
    }

    public SudokuCell getItem( int position ){
        int x = position % 9;
        int y = position / 9;

        return Sudoku[x][y];
    }

    public void setItem( int x , int y , int number ){
        Sudoku[x][y].setValue(number);
    }

    public void checkGame(){
        int [][] sudGrid = new int[9][9];
        for( int x = 0 ; x < 9 ; x++ ){
            for( int y = 0 ; y < 9 ; y++ ){
                sudGrid[x][y] = getItem(x,y).getValue();
            }
        }

        SudokuChecker.getInstance().checkSudoku(sudGrid, context);

//        if( SudokuChecker.getInstance().checkSudoku(sudGrid)){
//            //Toast.makeText(context, "Selamat anda berhasil menyelesaikan game ini.", Toast.LENGTH_LONG).show();
//            MainActivity activity = MainActivity.instance;
//            if (activity!=null){
//                activity.selesaiMain();
//            }
//        } else {
//            Toast.makeText(context, "ANDA GAGAL", Toast.LENGTH_LONG).show();
//        }

    }
}