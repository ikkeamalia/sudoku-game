package com.example.ikkeamalia.filmlist.view.sudokugrid;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

import com.example.ikkeamalia.filmlist.GameEngine;

/**
 * Created by Krisnanda on 17/03/2019.
 */

public class SudokuCell extends BaseSudokuCell {

    private Paint mPaint;

    public boolean odd;
    public String clickedCell;

    public boolean clicked=false;
    int x,y;

    public SudokuCell( Context context , boolean odd , int x , int y) {
        super(context);

        this.odd = odd;
        this.x = x;
        this.y=y;
        mPaint = new Paint();

//        if (!clickedCell.equalsIgnoreCase("cellClicked")) {
//            mPaint = new Paint();
//        }


       // System.out.println("Called"+"sudokucell"+clickedCell +" context"+context);

    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //GameEngine.getInstance().getGrid().getItem(1).drawBackGround(canvas);
        //drawBackGroundBlue(canvas);
        System.out.println("Called"+" ondraw"+clickedCell );
        if (odd) {
            drawBackGroundLightBlue(canvas);
        } else {
            drawBackGroundBlue(canvas);
            //drawLines(canvas);
        }
        drawNumber(canvas);

    }

    public void drawLineOrNot(Canvas canvas){
        System.out.println("Called"+" ondraw"+clicked );
        if (clicked){
            drawLines(canvas);
        }
    }


    private void drawNumber(Canvas canvas){
        mPaint.setColor(Color.WHITE);
        mPaint.setTextSize(50);
        mPaint.setStyle(Paint.Style.FILL);

        Rect bounds = new Rect();
        mPaint.getTextBounds(String.valueOf(getValue()), 0, String.valueOf(getValue()).length(), bounds);

        if( getValue() != 0 ){
            canvas.drawText(String.valueOf(getValue()), (getWidth() - bounds.width())/2, (getHeight() + bounds.height())/2	, mPaint);
        }
    }

    private void drawLines(Canvas canvas) {
        System.out.println("Called"+"drawline");
        mPaint.setColor(Color.parseColor("#FF8C00"));
        mPaint.setStrokeWidth(4);
        mPaint.setStyle(Paint.Style.STROKE);

        canvas.drawRect(0, 0, 0, 0, mPaint);


    }

    private void drawBackGroundBlue(Canvas canvas){
       // mPaint.setColor(Color.parseColor("#324d5c"));

       // if (GameEngine.getInstance().getGrid().getItem(1).drawBackGround();)
        canvas.drawColor(Color.parseColor("#324d5c"));
    }

    private void drawBackGroundLightBlue(Canvas canvas){
        // mPaint.setColor(Color.parseColor("#324d5c"));

        // if (GameEngine.getInstance().getGrid().getItem(1).drawBackGround();)
        canvas.drawColor(Color.parseColor("#3b5767"));
    }
}
