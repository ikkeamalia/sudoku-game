package com.example.ikkeamalia.filmlist;

import android.content.Context;
import android.content.SharedPreferences;

public class Preference {

    private static Preference instance;

    public static Preference getInstance(){
        if( instance == null ){
            instance = new Preference();
        }
        return instance;
    }

    public void setArrayPrefs( int[][] Sudoku, Context mContext) {
        SharedPreferences prefs = mContext.getSharedPreferences("sudoku", 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("sudoku_size", Sudoku.length);
        for (int i=0; i<Sudoku.length; i++){
            for (int j=0; j<Sudoku.length; j++){
                editor.putInt("sudoku_"+"i"+ i+"j"+j, Sudoku[i][j]);
                System.out.println("sudoku_"+"i"+ i+"j"+j+ Sudoku[i][j]);
            }

        }

//        for(int i=0;i<array.size();i++)
//            editor.putString(arrayName + "_" + i, array.get(i));
        editor.apply();
    }

    public void setLastTick(long tick, Context mContext){
        SharedPreferences prefs = mContext.getSharedPreferences("tick", 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putLong("tick", tick);

        editor.apply();
    }

    public long getLastTick(Context mContext){
        SharedPreferences prefs = mContext.getSharedPreferences("tick", 0);
        long tick = prefs.getLong("tick", 0);

        return tick;
    }

    public int[][] getArrayPrefs(Context mContext) {
        SharedPreferences prefs = mContext.getSharedPreferences("sudoku", 0);
        int size = prefs.getInt("sudoku_size", 0);

        int[][] SudokuFromPf = new int[size][size];
        for (int i=0; i<size; i++){
            for (int j=0; j<size; j++){
                SudokuFromPf[i][j] = prefs.getInt("sudoku_"+"i"+ i+"j"+j,0);
            }

        }

        return SudokuFromPf;
    }
}
