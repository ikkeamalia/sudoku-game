package com.example.ikkeamalia.filmlist;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Krisnanda on 17/03/2019.
 */

public class SudokuChecker {
    private static SudokuChecker instance;

    private SudokuChecker(){}
    int flagNotFull=0;

    public static SudokuChecker getInstance(){
        if( instance == null ){
            instance = new SudokuChecker();
        }
        return instance;
    }

    public void checkSudoku( int[][] Sudoku, Context context){
        flagNotFull=0;
        boolean cekAllCells=false;
        for (int i=0; i<Sudoku.length; i++){
            for (int j=0; j<Sudoku.length; j++){
                if (Sudoku[i][j]==0){
                    flagNotFull=1;
                }
            }
        }

        cekAllCells = checkHorizontal(Sudoku)  && checkVertical(Sudoku)  && checkRegions(Sudoku);

        if (flagNotFull==0 && cekAllCells){
            MainActivity activity = MainActivity.instance;
            if (activity!=null){
                activity.selesaiMain();
            }
        } else if ((flagNotFull==1 && !cekAllCells) || (flagNotFull==0 && !cekAllCells) ){
            MainActivity activity = MainActivity.instance;
            if (activity!=null){
                activity.gameOver();
            }
        }
       // return (checkHorizontal(Sudoku)  && checkVertical(Sudoku)  && checkRegions(Sudoku));
    }

    private boolean checkHorizontal(int[][] Sudoku) {
        boolean ret=true;
        for( int y = 0 ; y < 9 ; y++ ){
            for( int xPos = 0 ; xPos < 9 ; xPos++ ){

//                if( Sudoku[xPos][y] == 0 ){
//                    ret = false;
//                    return false;
//                }

                for( int x = xPos + 1 ; x < 9 ; x++ ){
                    if( Sudoku[xPos][y] == Sudoku[x][y] && Sudoku[x][y] != 0 ){
                        ret = false;
                        return false;
                    }
                }


            }
        }

        System.out.println("CEKHOR"+ret);
        return true;
    }

    private boolean checkVertical(int[][] Sudoku) {
        boolean ret=true;
        for( int x = 0 ; x < 9 ; x++ ){
            for( int yPos = 0 ; yPos < 9 ; yPos++ ){

//                if( Sudoku[x][yPos] == 0 ){
//                    ret=false;
//                    return false;
//                }
                for( int y = yPos + 1 ; y < 9 ; y++ ){
                    if( Sudoku[x][yPos] == Sudoku[x][y] && Sudoku[x][y] != 0 ){
                        ret=false;
                        return false;
                    }
                }
            }
        }

        System.out.println("CEKVER"+ret);
        return true;
    }

    private boolean checkRegions(int[][] Sudoku) {
        for( int xRegion = 0; xRegion < 3; xRegion++ ){
            for( int yRegion = 0; yRegion < 3 ; yRegion++ ){
                if( !checkRegion(Sudoku, xRegion, yRegion) ){
                    return false;
                }
            }
        }

        return true;
    }

    private boolean checkRegion(int[][] Sudoku , int xRegion , int yRegion){
        boolean ret=true;
        for( int xPos = xRegion * 3; xPos < xRegion * 3 + 3 ; xPos++ ){
            for( int yPos = yRegion * 3 ; yPos < yRegion * 3 + 3 ; yPos++ ){
                for( int x = xPos ; x < xRegion * 3 + 3 ; x++ ){
                    for( int y = yPos ; y < yRegion * 3 + 3 ; y++ ){
                        if( (( x != xPos || y != yPos) && Sudoku[xPos][yPos] == Sudoku[x][y] ) && Sudoku[x][y] != 0 ){
                            ret=false;
                            return false;

                        }
                    }
                }
            }
        }

        System.out.println("CEKREG"+ret);
        return true;
    }
}
