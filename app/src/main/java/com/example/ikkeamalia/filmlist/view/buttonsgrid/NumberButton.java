package com.example.ikkeamalia.filmlist.view.buttonsgrid;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.support.v7.widget.AppCompatTextView;
import android.widget.TextView;
import android.widget.Toast;


import com.example.ikkeamalia.filmlist.GameEngine;
import com.example.ikkeamalia.filmlist.MainActivity;

/**
 * Created by Krisnanda on 17/03/2019.
 */

public class NumberButton extends AppCompatTextView implements View.OnClickListener {

    private int number;

    public NumberButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (!MainActivity.timerPaused) {
            GameEngine.getInstance().setNumber(number);
        } else {
            Toast.makeText(getContext(), "Nyalakan countdown timer terlebih dahulu", Toast.LENGTH_SHORT).show();
        }
    }

    public void setNumber(int number){
        this.number = number;
    }

}
